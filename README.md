# RETO CLM DESARROLLO

Reto para CLM Digital Solutions

## Author

- [@yzz0f](https://www.github.com/yzz0f)

## API Reference

#### Swagger Documentation

```http
  GET /swagger
```


#### Get all movies

```http
  GET /movies
```

| Parameter | Type      | Description   |
| :-------- | :-------- | :------------ |
| `page`    | `numeric` | **In Header** |

#### Get movie

```http
  GET /movies/${searchValue}
```

| Parameter     | Type     | Description                  |
| :------------ | :------- | :--------------------------- |
| `searchValue` | `string` | **Required**. Title of movie |

#### Get plot

```http
  POST /movies/
```

| Parameter | Type     | Description                  |
| :-------- | :------- | :--------------------------- |
| `movie`   | `string` | **Required**. Title of movie |
| `search`  | `string` | **Required**. Word to find   |
| `replace` | `string` | **Required**. Replace Word   |

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`NODE_ENV=development`  
`PORT=4000`  
`API_URL=http://www.omdbapi.com`  
`API_KEY=5bf64717`  
`DB_URI=mongodb://mongo/movies`

## Run Locally

Clone the project

GITHUB
```bash
  git clone https://github.com/yzz0f/RetoCLM
```
 OR GITLAB
```bash
  git clone https://gitlab.com/fernando182/RetoCLM
```

Go to the project directory

```bash
  cd RetoCLM
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run start
```

## Run in Docker

Clone the project

```bash
  git clone https://github.com/yzz0f/RetoCLM
```

Go to the project directory

```bash
  cd RetoCLM
```

Build Docker File

```bash
  docker-compose build
```

Up Docker Image

```bash
  docker-compose up
```
