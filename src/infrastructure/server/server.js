const Koa = require("koa");
const swagger = require("swagger2");
const bodyparser = require("koa-bodyparser");
const Router = require("koa-router");
const logger = require("koa-logger");
const cors = require("@koa/cors");
const { ui, validate } = require("swagger2-koa");

const { errorHandler } = require("./middlewares/error-handler");
const { loadRoutes } = require("./routes");
const swaggerDocument = swagger.loadDocumentSync(
    `${__dirname}/docs/swagger.yaml`
);

function loadWebServer({ port }) {
    const app = new Koa();

    const router = new Router();
    loadRoutes(router);

    app
        .use(errorHandler)
        .use(cors())
        .use(bodyparser())
        .use(logger())
        .use(router.routes())
        .use(router.allowedMethods())
        .use(ui(swaggerDocument, "/swagger"));

    let runningServer = undefined;
    return {
        start: () => {
            runningServer = app.listen(port, () =>
                console.log(`- Web server started on PORT: ${port}`)
            );
        },
        close: () => {
            runningServer && runningServer.close();
            console.log("- Closed web server");
        },
    };
}

module.exports = { loadWebServer };