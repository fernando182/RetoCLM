const errorHandler = async(ctx, next) => {
    try {
        await next();
    } catch (error) {
        console.log(error);
        ctx.status = error.statusCode || 500;
        ctx.body = {
            timeStamp: Date.now(),
            statusCode: ctx.status,
            ...(ctx.status === 500 ?
                {
                    message: error.message || "Internal Server Error",
                    details: "No details available",
                } :
                {
                    message: error.message,
                    details: error.toJSON() || {},
                }),
        };
    }
};

module.exports = { errorHandler };