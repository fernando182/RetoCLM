const Joi = require("joi");

const validateBody = async(ctx, next) => {
    const configSchema = Joi.object({
        movie: Joi.string().required(),
        search: Joi.string().required(),
        replace: Joi.string().required(),
    });
    const { error, value } = configSchema.validate(ctx.request.body);
    console.error({ error, value });

    if (error) {
        ctx.statusCode = 400;
        ctx.body = {
            timeStamp: Date.now(),
            statusCode: 400,
            message: "Bad Request",
            details: error.details,
        };
    } else {
        await next();
    }
    //await next();
};
module.exports = { validateBody };