const { pong } = require("../controllers/ping");
const {
    getMovies,
    searchMovie,
    updateMovies,
} = require("../controllers/movies");

const { validateBody } = require("../middlewares/validate-body");

function loadRoutes(router) {
    router.get("/ping", pong);
    router.get("/movies", getMovies);
    router.get("/movies/:search", searchMovie);
    router.post("/movies", validateBody, updateMovies);
    return router;
}

module.exports = { loadRoutes };