const {
    searchApiMovie,
    createMovie,
    getAllMovies,
    findMovie,
} = require("../services/movies");

const getMovies = async(ctx) => {
    try {
        const page = ctx.headers.page || 1;
        const limit = 5;
        const movies = await getAllMovies({ page, limit });
        return (ctx.body = movies);
    } catch (error) {
        console.error(error);
    }
};

const searchMovie = async(ctx) => {
    const search = ctx.params.search;
    const year = ctx.headers.year || "";

    const movie = await searchApiMovie({ search, year });
    if (movie.Response === "False") {
        return (ctx.body = "Movie not found");
    }
    const newMovie = {
        Title: movie.Title,
        Year: movie.Year,
        Released: movie.Released,
        Genre: movie.Genre,
        Director: movie.Director,
        Actors: movie.Actors,
        Plot: movie.Plot,
        Ratings: movie.Ratings,
    };

    const createdMovie = await createMovie(newMovie);

    return (ctx.body = createdMovie);
};

const updateMovies = async(ctx) => {
    const { movie, search, replace } = ctx.request.body;
    const { Plot = "" } = await findMovie(movie);
    const re = new RegExp(search, "g");
    return (ctx.body = Plot.replace(re, replace));
};

module.exports = { getMovies, searchMovie, updateMovies };