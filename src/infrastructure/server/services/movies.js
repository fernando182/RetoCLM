const axios = require("axios");
const moviesModel = require("../../models/Movies");
const { loadConfig } = require("../../config");
const config = loadConfig();
const searchApiMovie = async({ search, year }) => {
    try {
        const response = await axios.get(
            `${config.API_URL}/?t=${search}&y=${year}&apikey=${config.API_KEY}`, {}
        );
        return response.data;
    } catch (error) {
        console.error(error);
    }
};

const getAllMovies = async({ page, limit }) => {
    const movies = await moviesModel
        .find({})
        .skip((page - 1) * limit)
        .limit(limit);
    return movies;
};

const findMovie = async(Title) => {
    const movie = await moviesModel.findOne({
        Title: { $regex: new RegExp("^" + Title.toLowerCase(), "i") },
    });
    return movie;
};
const createMovie = async(movie) => {
    const exist = await findMovie(movie.Title);
    if (!exist) {
        return await moviesModel.create(movie);
    }
    return exist;
};

module.exports = { searchApiMovie, createMovie, findMovie, getAllMovies };