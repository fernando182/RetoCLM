const mongoose = require("mongoose");
const MoviesSchema = mongoose.Schema({
    Title: {
        type: String,
        required: true,
    },
    Year: {
        type: Number,
    },
    Released: {
        type: String,
        required: true,
    },
    Genre: {
        type: String,
        required: true,
    },
    Director: {
        type: String,
        required: true,
    },
    Actors: {
        type: String,
        required: true,
    },
    Plot: {
        type: String,
        required: true,
    },
    Ratings: [{
        Source: String,
        Value: String,
    }, ],
});

module.exports = mongoose.model("Movie", MoviesSchema);